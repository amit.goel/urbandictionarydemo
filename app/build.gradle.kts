plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
    id("androidx.navigation.safeargs")
    id("org.jmailen.kotlinter") version "2.2.0"
}

android {
    buildToolsVersion(Apps.buildTools)
    compileSdkVersion(Apps.compileSdk)
    defaultConfig {
        applicationId = Apps.applicationId
        minSdkVersion(Apps.minSdk)
        targetSdkVersion(Apps.targetSdk)
        versionCode = Apps.versionCode
        versionName = Apps.versionName

        buildConfigField("String", "HOST", Hosts.baseUrl)
        buildConfigField("String", "LAST_FM_HOST", Hosts.lastFmBaseUrl)
        buildConfigField("String", "URBAN_API_KEY", Hosts.urbanApiKey)

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {

        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    lintOptions {
        isAbortOnError = false
        isCheckReleaseBuilds = false
        disable("ContentDescription")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    testOptions {
        unitTests.isIncludeAndroidResources = true
    }

    androidExtensions {
        isExperimental = true
    }

    viewBinding {
        isEnabled = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Libs.kotlin)

    implementation(Libs.appcompat)
    implementation(Libs.constraintLayout)
    implementation(Libs.coreKtx)
    implementation(Libs.fragment)
    implementation(Libs.material)
    implementation(Libs.navigation)
    implementation(Libs.navigationUI)

    // Lifecycle Extensions
    implementation(Libs.lifecycleExtensions)
    implementation(Libs.lifecycleLiveData)
    implementation(Libs.lifecycleViewModel)
    implementation(Libs.lifecycleRuntime)

    implementation(Libs.retrofit)
    implementation(Libs.retrofitMoshi)
    implementation(Libs.okHttpInterceptor)

    // Kotlin coroutines
    implementation(Libs.coroutines)
    implementation(Libs.coroutinesCore)

    // Koin
    implementation(Libs.koin)
    implementation(Libs.koinScope)
    implementation(Libs.koinViewModel)

    // Room
    implementation(Libs.room)
    implementation(Libs.roomRuntime)
    kapt(Libs.roomKapt)

    // Image Handling
    implementation(Libs.glide)
    kapt(Libs.glideCompiler)

    testImplementation(TestLibs.junit)
    testImplementation(TestLibs.coroutinesTest)
    testImplementation(TestLibs.koinTest)
    testImplementation(TestLibs.mockWebServer)

    androidTestImplementation(TestLibs.junitAndroid)
    androidTestImplementation(TestLibs.espresso)
    androidTestImplementation(TestLibs.fragmentTest)

    // Android runner and rules support
    androidTestImplementation(TestLibs.testCore)
    androidTestImplementation(TestLibs.testRunner)
    androidTestImplementation(TestLibs.testRules)
}

kotlinter {
    disabledRules = arrayOf("import-ordering", "no-wildcard-imports", "filename")
}

val ktLint by tasks.creating(org.jmailen.gradle.kotlinter.tasks.LintTask::class) {
    group = "verification"
    source(files("src"))
    reports = mapOf(
        "plain" to file("build/lint-report.txt"),
        "json" to file("build/lint-report.json")
    )
    ignoreFailures = false
}

tasks.getByName("build").finalizedBy(ktLint)
