package com.ghn.urbandictionarydemo.data.local

import com.ghn.urbandictionarydemo.data.base.UrbanLocalDataSource
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule

class FakeLocalDataSource : UrbanLocalDataSource {
    override suspend fun getDefinitions(query: String): List<DefinitionModule> {
        return listOf(
            DefinitionModule(
                id = 1,
                thumbsDown = 212,
                term = "moonshot",
                permalink = "",
                example = "hello",
                definition = "hello all my friends",
                current_vote = "",
                author = "me",
                soundUrls = "",
                thumbsUp = 454,
                writtenOn = "2001-01-02"
            ),
            DefinitionModule(
                id = 2,
                thumbsDown = 2121,
                term = "moonshot",
                permalink = "",
                example = "hello2",
                definition = "hello2 all my friends",
                current_vote = "",
                author = "me",
                soundUrls = "",
                thumbsUp = 4542,
                writtenOn = "2001-01-02"
            ),
            DefinitionModule(
                id = 4,
                thumbsDown = 2121,
                term = "moonshot",
                permalink = "",
                example = "hello2",
                definition = "hello2 all my friends",
                current_vote = "",
                author = "me",
                soundUrls = "",
                thumbsUp = 4542,
                writtenOn = "2001-01-02"
            )
        )
    }

    override suspend fun saveDefinitions(definitions: List<DefinitionModule>) {
    }
}
