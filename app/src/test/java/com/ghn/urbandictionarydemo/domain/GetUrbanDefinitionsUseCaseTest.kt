package com.ghn.urbandictionarydemo.domain

import com.ghn.urbandictionarydemo.di.testNetworkModule
import com.ghn.urbandictionarydemo.di.testUrbanModule
import com.ghn.urbandictionarydemo.shared.util.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.qualifier.named
import org.koin.test.KoinTest
import org.koin.test.inject
import java.net.HttpURLConnection

@ExperimentalCoroutinesApi
class GetUrbanDefinitionsUseCaseTest : KoinTest {

    private val mockWebServer by inject<MockWebServer>()
    private val testDispatcher by inject<TestCoroutineDispatcher>(
        named("testCoroutineDispatcher")
    )

    private val getUrbanDefinitionsUseCase by inject<GetUrbanDefinitionsUseCase>(
        named("GetUrbanDefinitionsUseCaseTest")
    )

    private lateinit var mockResponse: MockResponse

    private val successMessageBody: String = """
		{
        "list": [
            {
                "definition": "awesome, fantastic, almost impossible to achieve, the best, the reason for success, [reaching] the highest point, [right on] [target]",
                "permalink": "http://moonshot.urbanup.com/5083184",
                "thumbs_up": 109,
                "sound_urls": [],
                "author": "AnonymousHelp",
                "word": "Moonshot",
                "defid": 5083184,
                "current_vote": "",
                "written_on": "2010-07-08T00:00:00.000Z",
                "example": "This new product was the [moonshot] for our company. It was extremely successful and sent our [sales] [to the moon].",
                "thumbs_down": 6
            },
            {
                "definition": "When at [the brink] of male ejaculation, you or a friend inserts their finger or [foreign object] into the male’s anal cavity causing extreme pressure and force during the cumshot, shooting your cum [to the moon].",
                "permalink": "http://moonshot.urbanup.com/13637665",
                "thumbs_up": 9,
                "sound_urls": [],
                "author": "Poopyface1",
                "word": "moonshot",
                "defid": 13637665,
                "current_vote": "",
                "written_on": "2019-02-20T00:00:00.000Z",
                "example": "Man, [my girl] gave me the best moonshot last night. I broke [the light] on her [ceiling]!!",
                "thumbs_down": 3
            },
            {
                "definition": "adjetive. Something intended to be genuinely awesome, but is only so in its ironic appreciation. ie, something that's \"so bad, it's good\". Must be [sincere] and lacking irony in its intent. [Parody], therefore, is not moonshot. It derives from the [notion] that it's something that's \"shooting for the moon\".But because it's so awful, it actually becomes awesome.Things that are moonshot usually have a cult following.  ",
                "permalink": "http://moonshot.urbanup.com/2714692",
                "thumbs_up": 8,
                "sound_urls": [],
                "author": "Anonimatous",
                "word": "moonshot",
                "defid": 2714692,
                "current_vote": "",
                "written_on": "2007-11-28T00:00:00.000Z",
                "example": "\"[Plan 9] from [Outer Space]\" is [classic] moonshot.",
                "thumbs_down": 15
            },
            {
                "definition": "adj. Describing a thing, person, etc, that is ridiculous/lame, but awesome/[enjoyable] at the same time. In other words, something that's so bad, it's actually good. It usually applies to something that is genuinely trying to be good, yet is ridiculously bad. So bad that it's enjoyable. While the appreciation for the quality of a certain thing is [subjective], it must be [universally] enjoyed for its ridiculous/lame aspects to be considered moonshot. ",
                "permalink": "http://moonshot.urbanup.com/2689145",
                "thumbs_up": 9,
                "sound_urls": [],
                "author": "Anonymous3005",
                "word": "moonshot",
                "defid": 2689145,
                "current_vote": "",
                "written_on": "2007-11-13T00:00:00.000Z",
                "example": "[Showgirls] (the movie) is totally moonshot.\r\n[Sanjaya Malakar] from [American Idol] gave a moonshot performance. ",
                "thumbs_down": 19
            },
            {
                "definition": "When you vomit into [the blow hole] of a whale that has [surfaced] and it then violently evacuates the vomit from the blow hole into the air, covering all nearby [whale watchers].",
                "permalink": "http://moonshot.urbanup.com/5103735",
                "thumbs_up": 5,
                "sound_urls": [],
                "author": "Willis1137",
                "word": "Moonshot",
                "defid": 5103735,
                "current_vote": "",
                "written_on": "2010-07-18T00:00:00.000Z",
                "example": "Ew! Andrew P. just [puked] on that [whale] and I got covered by the [moonshot]!",
                "thumbs_down": 20
            },
            {
                "definition": "when a man stands back to back with a woman and [arcs] his [cumshot] over the two of them making it resemble a [shooting star]",
                "permalink": "http://moonshot.urbanup.com/4134696",
                "thumbs_up": 2,
                "sound_urls": [],
                "author": "cliff irishman",
                "word": "moonshot",
                "defid": 4134696,
                "current_vote": "",
                "written_on": "2009-07-25T00:00:00.000Z",
                "example": "dude that chick was so crazy she let me moonshot her when it [got over] [the two] of us it hit her in the [toes]",
                "thumbs_down": 21
            },
            {
                "definition": "when a man and a woman stand back to back and then man [arcs] his cum shot over them both like a [shooting star] and they both [make a wish]",
                "permalink": "http://moonshot.urbanup.com/4126839",
                "thumbs_up": 2,
                "sound_urls": [],
                "author": "cliff poopdick",
                "word": "moonshot",
                "defid": 4126839,
                "current_vote": "",
                "written_on": "2009-07-22T00:00:00.000Z",
                "example": "moonshot is [greatness]",
                "thumbs_down": 21
            },
            {
                "definition": "[Shots] of [Moonshine]",
                "permalink": "http://moonshots.urbanup.com/4360809",
                "thumbs_up": 4,
                "sound_urls": [],
                "author": "The Master Dutch",
                "word": "Moonshots",
                "defid": 4360809,
                "current_vote": "",
                "written_on": "2009-11-11T00:00:00.000Z",
                "example": "\"Why are you guys [curled] up [on the floor] [like that]? Did you drink too many Moonshots?",
                "thumbs_down": 9
            },
            {
                "definition": "Something absolutely epic, unbelievable and/or awe-inspiring. Named for [Albert Pujols's] massive (estimated) 468-foot home run in Game 5 in the 2005 NLCS off [Astros] pitcher [Brad Lidge] when the Cardinals were down to their last out.\n\nCan be used to describe any amazing achievement anywhere. Does not necessary have to be clutch, but can be.",
                "permalink": "http://pujols-moonshot.urbanup.com/11647422",
                "thumbs_up": 0,
                "sound_urls": [],
                "author": "Ten Forward",
                "word": "pujols moonshot",
                "defid": 11647422,
                "current_vote": "",
                "written_on": "2017-06-04T00:00:00.000Z",
                "example": "\"Michael J. Fox truly hit a [Pujols moonshot] with his wife.\"\r\n\"Did you see Jamie Foxx in Ray? That was a Pujols moonshot performance if ever I saw one\"\r\n\"Eisenhower. D-Day. Pujols moonshot. End of discussion.\"\r\n\"[The Knack] didn't have the best career, but dammit, My [Sharona] was a Pujols moonshot for the ages.'",
                "thumbs_down": 0
            },
            {
                "definition": "An alcoholic shooter invented by [cryptocurrency] enthusiasts at the El Rio bar in San Francisco.\n\nThe recipe is half white rum, half [Fernet] with a crushed cherry at the bottom of the shot glass.\n\nDan Held from [Blockchain].info suggested Pop Rocks replace the crushed cherry but they weren't available during conception.",
                "permalink": "http://moonshot.urbanup.com/9277243",
                "thumbs_up": 0,
                "sound_urls": [],
                "author": "davidapple",
                "word": "Moonshot",
                "defid": 9277243,
                "current_vote": "",
                "written_on": "2016-09-06T00:00:00.000Z",
                "example": "Hey Jackson [Palmer], you got some [dogecoin] for a [moonshot]?",
                "thumbs_down": 0
            }
        ]
    }
	""".trimIndent()

    @Before
    fun setup() {
        startKoin {
            modules(listOf(testNetworkModule, testUrbanModule))
        }

        Dispatchers.setMain(testDispatcher)
        mockWebServer.start()

        mockResponse = MockResponse()
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()

        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
        stopKoin()
    }

    @Test
    fun execute() = runBlocking {

        mockResponse.setResponseCode(HttpURLConnection.HTTP_OK)
            .addHeader("content-type: application/json; charset=utf-8")
            .setBody(successMessageBody)

        mockWebServer.enqueue(mockResponse)

        val result = getUrbanDefinitionsUseCase("moonshot" to true)
        assertTrue(result is Result.Success)
        assertTrue((result as Result.Success).data.size == 3)
    }

    @Test
    fun `test repository network error`() = runBlocking {

        mockResponse.setResponseCode(HttpURLConnection.HTTP_GATEWAY_TIMEOUT)
            .addHeader("content-type: application/json; charset=utf-8")

        mockWebServer.enqueue(mockResponse)

        val result = getUrbanDefinitionsUseCase("2020-01-02" to true)
        assertTrue(result is Result.Error)
        assertTrue((result as Result.Error).throwable.message == "Remote Failure")
    }
}
