package com.ghn.urbandictionarydemo.di

import com.ghn.urbandictionarydemo.data.api.UrbanApi
import com.ghn.urbandictionarydemo.data.local.FakeLocalDataSource
import com.ghn.urbandictionarydemo.data.remote.UrbanRemoteDataSourceImpl
import com.ghn.urbandictionarydemo.data.repository.UrbanRepositoryImpl
import com.ghn.urbandictionarydemo.domain.GetUrbanDefinitionsUseCase
import com.ghn.urbandictionarydemo.shared.extensions.createRetrofit
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

val testUrbanModule = module {

    single(named("testUrbanAPI")) { provideTestUrbanAPI(get(named("testRetrofit"))) }

    single(named("remoteDataSourceFake")) {
        UrbanRemoteDataSourceImpl(get(named("testUrbanAPI")))
    }

    single(named("localDataSourceFake")) {
        FakeLocalDataSource()
    }

    single(named("urbanRepositoryTestFakes")) {
        UrbanRepositoryImpl(
            get(named("localDataSourceFake")),
            get(named("remoteDataSourceFake"))
        )
    }

    factory(named("GetUrbanDefinitionsUseCaseTest")) {
        GetUrbanDefinitionsUseCase(
            get(named("urbanRepositoryTestFakes")),
            get(named("testCoroutineDispatcher"))
        )
    }
}

fun provideTestUrbanAPI(retrofit: Retrofit): UrbanApi = retrofit.createRetrofit()
