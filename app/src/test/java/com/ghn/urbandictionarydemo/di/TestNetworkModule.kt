package com.ghn.urbandictionarydemo.di

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockWebServer
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
val testNetworkModule = module {

    single {
        MockWebServer()
    }

    single(named("testCoroutineDispatcher")) {
        TestCoroutineDispatcher()
    }

    single(named("testOkHttpClient")) {
        OkHttpClient.Builder()
            .connectTimeout(500, TimeUnit.MILLISECONDS)
            .writeTimeout(500, TimeUnit.MILLISECONDS)
            .readTimeout(500, TimeUnit.MILLISECONDS)
            .addInterceptor(HttpLoggingInterceptor())
            .build()
    }

    single(named("testRetrofit")) {
        provideTestRetrofit(
            mockWebServer = get(),
            okHttpClient = get(named("testOkHttpClient"))
        )
    }
}

fun provideTestRetrofit(
    mockWebServer: MockWebServer,
    okHttpClient: OkHttpClient
): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .baseUrl(mockWebServer.url("/"))
        .client(okHttpClient)
        .build()
}
