package com.ghn.urbandictionarydemo.ui.lookup

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.ghn.urbandictionarydemo.MainActivity
import com.ghn.urbandictionarydemo.R
import com.ghn.urbandictionarydemo.shared.extensions.isNetworkAvailable
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4ClassRunner::class)
class UrbanDictionaryFragmentTest {

    @Rule
    @JvmField
    var activityActivityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Before
    fun setUp() {
        activityActivityTestRule.activity.supportFragmentManager.beginTransaction()
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testSearchView() {

        val query = "theranos"

        onView(withId(R.id.search)).check(matches((isDisplayed())))

        onView(withId(R.id.search)).perform(clearText(), typeText(query))

        onView(withId(R.id.search_term)).check(matches((isDisplayed())))

        val textQueryFormat =
            activityActivityTestRule.activity.getString(R.string.search_term_formatter, query)

        val networkError =
            activityActivityTestRule.activity.getString(R.string.network_not_connected)

        when {
            activityActivityTestRule.activity.isNetworkAvailable() -> {
                onView(withId(R.id.search_term)).check(matches(withText(textQueryFormat)))
            }
            else -> {
                onView(withId(R.id.search_term)).check(matches(withText(networkError)))
            }
        }
    }
}
