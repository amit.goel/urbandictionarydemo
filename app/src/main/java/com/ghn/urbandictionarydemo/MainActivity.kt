package com.ghn.urbandictionarydemo

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ghn.urbandictionarydemo.databinding.ActivityMainBinding
import com.ghn.urbandictionarydemo.ui.lookup.LastFmFragment
import com.ghn.urbandictionarydemo.ui.lookup.UrbanDictionaryFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@FlowPreview
@ExperimentalCoroutinesApi
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction()
            .add(R.id.nav_host_fragment, UrbanDictionaryFragment.newInstance(), URBAN_FRAGMENT)
            .commit()

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            val newFragmentPair: Pair<Fragment?, String>? = getFragment(item)
            newFragmentPair?.let { pair ->
                pair.first?.let { fragment ->
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.nav_host_fragment, fragment, pair.second)
                        .commit()
                }
            }
            true
        }
    }

    private fun getFragment(item: MenuItem): Pair<Fragment?, String>? {
        val currentFragment = currentVisibleFragment()
        return when (item.itemId) {
            R.id.menu_bottom_nav_urban -> {
                if (currentFragment !is UrbanDictionaryFragment) {
                    (supportFragmentManager.findFragmentByTag(URBAN_FRAGMENT)
                        ?: UrbanDictionaryFragment.newInstance()) to URBAN_FRAGMENT
                } else {
                    null
                }
            }
            R.id.menu_bottom_nav_last_fm -> {
                if (currentFragment !is LastFmFragment) {
                    (supportFragmentManager.findFragmentByTag(LAST_FM_FRAGMENT)
                        ?: LastFmFragment.newInstance()) to LAST_FM_FRAGMENT
                } else {
                    null
                }
            }
            else -> null
        }
    }

    private fun currentVisibleFragment(): Fragment? {
        return supportFragmentManager.fragments.firstOrNull()
    }

    companion object {
        const val URBAN_FRAGMENT = "urban_fragment"
        const val LAST_FM_FRAGMENT = "last_fm_fragment"
    }
}
