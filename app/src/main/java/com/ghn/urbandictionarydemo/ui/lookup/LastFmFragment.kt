package com.ghn.urbandictionarydemo.ui.lookup

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.ghn.urbandictionarydemo.R
import com.ghn.urbandictionarydemo.data.models.response.tracksearch.Track
import com.ghn.urbandictionarydemo.data.models.response.tracksearch.TrackSearchResponse
import com.ghn.urbandictionarydemo.shared.base.BaseLookupFragment
import com.ghn.urbandictionarydemo.shared.extensions.isNetworkAvailable
import com.ghn.urbandictionarydemo.shared.util.Result
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@FlowPreview
class LastFmFragment : BaseLookupFragment() {

    private val adapter by lazy { TrackSearchAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.definitionsRv.adapter = adapter
        viewModel.trackSearchQuery.value = getString(R.string.default_track_search)

        binding.search.hint = getString(R.string.last_fm_hint)
        setSearchArea(getString(R.string.default_track_search))

        binding.search.doOnTextChanged { text, _, _, _ ->
            viewModel.networkConnected = requireContext().isNetworkAvailable()
            viewModel.trackSearchQuery.value = text.toString()
            setSearchArea(text.toString())
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUpObservers() {
        viewModel.tracks.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> showLoading()
                is Result.Success -> renderView(result.data)
                is Result.Error -> showError()
            }
        }
    }

    private fun renderView(data: TrackSearchResponse) {
        viewLifecycleOwner.lifecycleScope.launch {
            delay(300)
            val items = data.results.trackMatches.track.toMutableList()
            items.sortItemsOnOptionsType()
            binding.progress.visibility = View.INVISIBLE
            adapter.submitList(items)
        }
    }

    override fun setUpSortDialog() {
        val dialog = BottomSheetDialog(requireActivity())
        val sheetView = layoutInflater.inflate(R.layout.fragment_filter_bottom_sheet, null)
        dialog.setContentView(sheetView)
        dialog.show()

        val sortOption = viewModel.sortOption.value ?: return
        dialog.findViewById<Chip>(SortOptions.MOST_LIKED.chipResId)?.text =
            getString(R.string.most_listened)
        dialog.findViewById<Chip>(SortOptions.MOST_DISLIKED.chipResId)?.text =
            getString(R.string.least_listened)

        val chipGroup = dialog.findViewById<ChipGroup>(R.id.feed_chip_group)
        chipGroup?.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                sortOption.chipResId -> dialog.dismiss() // Filter Option Unchanged
                else -> {
                    dialog.dismiss()
                    viewModel.sortOption.value = SortOptions.from(checkedId)

                    val items = adapter.currentList.toMutableList()
                    items.sortItemsOnOptionsType()
                    adapter.submitList(items)
                    binding.definitionsRv.smoothScrollBy(0, 0)
                }
            }
        }
    }

    private fun MutableList<Track>.sortItemsOnOptionsType() {
        when (viewModel.sortOption.value) {
            SortOptions.MOST_LIKED -> sortByDescending { it.listeners.toInt() }
            SortOptions.MOST_DISLIKED -> sortBy { it.listeners.toInt() }
        }
    }

    companion object {
        fun newInstance() = LastFmFragment()
    }
}
