package com.ghn.urbandictionarydemo.ui.lookup

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule
import com.ghn.urbandictionarydemo.databinding.ItemDefinitionBinding

class UrbanLookupAdapter :
    ListAdapter<DefinitionModule, UrbanLookupAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemDefinitionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    inner class ViewHolder(private val binding: ItemDefinitionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DefinitionModule) {
            with(binding) {
                definition.text = item.definition
                liked.text = item.thumbsUp.toString()
                disliked.text = item.thumbsDown.toString()
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<DefinitionModule>() {
        override fun areItemsTheSame(oldItem: DefinitionModule, newItem: DefinitionModule):
                Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: DefinitionModule, newItem: DefinitionModule):
                Boolean = oldItem == newItem
    }
}
