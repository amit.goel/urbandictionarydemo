package com.ghn.urbandictionarydemo.ui.lookup

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import com.ghn.urbandictionarydemo.R
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule
import com.ghn.urbandictionarydemo.shared.base.BaseLookupFragment
import com.ghn.urbandictionarydemo.shared.extensions.isNetworkAvailable
import com.ghn.urbandictionarydemo.shared.util.Result
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@FlowPreview
class UrbanDictionaryFragment : BaseLookupFragment() {

    private val adapter by lazy { UrbanLookupAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.definitionsRv.adapter = adapter
        viewModel.query.value = "moonshot"

        setSearchArea("moonshot")

        binding.search.doOnTextChanged { text, _, _, _ ->
            viewModel.networkConnected = requireContext().isNetworkAvailable()
            viewModel.query.value = text.toString()
            setSearchArea(text.toString())
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUpObservers() {
        viewModel.definitions.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> showLoading()
                is Result.Success -> renderView(result.data)
                is Result.Error -> showError()
            }
        }
    }

    private fun renderView(data: List<DefinitionModule>) {
        viewLifecycleOwner.lifecycleScope.launch {
            delay(300)

            val items = data.toMutableList()
            items.sortItemsOnOptionsType()
            binding.progress.visibility = View.INVISIBLE
            adapter.submitList(items)
        }
    }

    override fun setUpSortDialog() {
        val dialog = BottomSheetDialog(requireActivity())
        val sheetView = layoutInflater.inflate(R.layout.fragment_filter_bottom_sheet, null)
        dialog.setContentView(sheetView)
        dialog.show()

        val sortOption = viewModel.sortOption.value ?: return
        dialog.findViewById<Chip>(sortOption.chipResId)?.isChecked = true

        val chipGroup = dialog.findViewById<ChipGroup>(R.id.feed_chip_group)
        chipGroup?.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                sortOption.chipResId -> dialog.dismiss() // Filter Option Unchanged
                else -> {
                    dialog.dismiss()
                    viewModel.sortOption.value = SortOptions.from(checkedId)

                    val items = adapter.currentList.toMutableList()
                    items.sortItemsOnOptionsType()
                    adapter.submitList(items)
                }
            }
        }
    }

    private fun MutableList<DefinitionModule>.sortItemsOnOptionsType() {
        when (viewModel.sortOption.value) {
            SortOptions.MOST_LIKED -> sortByDescending { it.thumbsUp }
            SortOptions.MOST_DISLIKED -> sortByDescending { it.thumbsDown }
        }
    }

    companion object {
        fun newInstance() = UrbanDictionaryFragment()
    }
}
