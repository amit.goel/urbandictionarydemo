package com.ghn.urbandictionarydemo.ui.lookup

import androidx.annotation.LayoutRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import com.ghn.urbandictionarydemo.R
import com.ghn.urbandictionarydemo.domain.GetTrackSearchUseCase
import com.ghn.urbandictionarydemo.domain.GetUrbanDefinitionsUseCase
import com.ghn.urbandictionarydemo.shared.util.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map

class SharedLookUpViewModel(
    private val getUrbanDefinitionsUseCase: GetUrbanDefinitionsUseCase,
    private val getTrackSearchUseCase: GetTrackSearchUseCase
) : ViewModel() {

    val query = MutableLiveData<String>()
    val trackSearchQuery = MutableLiveData<String>()
    var networkConnected: Boolean = true
    val sortOption = MutableLiveData<SortOptions>(SortOptions.MOST_LIKED)

    @FlowPreview
    @ExperimentalCoroutinesApi
    val definitions = query.asFlow()
        .debounce(300)
        .map { if (it.isEmpty()) "moonshot" else it.trim() }
        .distinctUntilChanged()
        .flatMapLatest {
            flowOf(
                Result.Loading,
                getUrbanDefinitionsUseCase(it to networkConnected)
            )
        }
        .asLiveData()

    @FlowPreview
    @ExperimentalCoroutinesApi
    val tracks = trackSearchQuery.asFlow()
        .debounce(300)
        .map { if (it.isEmpty()) "Learning to Fly" else it.trim() }
        .distinctUntilChanged()
        .flatMapLatest {
            flowOf(Result.Loading, getTrackSearchUseCase(it))
        }
        .asLiveData()
}

enum class SortOptions(@LayoutRes val chipResId: Int) {
    MOST_LIKED(R.id.filter_most_liked),
    MOST_DISLIKED(R.id.filter_most_disliked);

    companion object {
        fun from(search: Int): SortOptions =
            requireNotNull(values().find { it.chipResId == search }) {
                "No SortOptions with value $search"
            }
    }
}
