package com.ghn.urbandictionarydemo.ui.lookup

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ghn.urbandictionarydemo.R
import com.ghn.urbandictionarydemo.data.models.response.tracksearch.Track
import com.ghn.urbandictionarydemo.databinding.ItemTrackSearchBinding

class TrackSearchAdapter :
    ListAdapter<Track, TrackSearchAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemTrackSearchBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    inner class ViewHolder(private val binding: ItemTrackSearchBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Track) {
            with(binding) {
                trackName.text = item.name
                trackArtist.text = item.artist
                listeners.text = root.context.getString(R.string.listeners_count, item.listeners)
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<Track>() {
        override fun areItemsTheSame(oldItem: Track, newItem: Track): Boolean =
            oldItem.name == newItem.name

        override fun areContentsTheSame(oldItem: Track, newItem: Track): Boolean =
            oldItem == newItem
    }
}
