package com.ghn.urbandictionarydemo

import android.app.Application
import com.ghn.urbandictionarydemo.data.AppDatabase
import com.ghn.urbandictionarydemo.di.localModule
import com.ghn.urbandictionarydemo.di.networkModule
import com.ghn.urbandictionarydemo.di.urbanLookupModule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

@Suppress("SpellCheckingInspection")
class UrbanDictionaryApplication : Application() {

    @ExperimentalCoroutinesApi
    override fun onCreate() {
        super.onCreate()

        val koinModules = listOf(
            localModule,
            networkModule,
            urbanLookupModule
        )

        startKoin {
            androidLogger()
            androidContext(this@UrbanDictionaryApplication)
            modules(koinModules)
        }

        AppDatabase.getInstance(this).openHelper.writableDatabase
    }
}
