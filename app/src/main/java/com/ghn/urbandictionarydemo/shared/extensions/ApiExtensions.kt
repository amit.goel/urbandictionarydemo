package com.ghn.urbandictionarydemo.shared.extensions

import android.content.Context
import android.net.ConnectivityManager
import retrofit2.Retrofit

inline fun <reified T> Retrofit.createRetrofit(): T = create(T::class.java)

fun Context.isNetworkAvailable(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo?.isConnected == true
}
