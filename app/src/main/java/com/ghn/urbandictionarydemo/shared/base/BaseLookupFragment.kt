package com.ghn.urbandictionarydemo.shared.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ghn.urbandictionarydemo.R
import com.ghn.urbandictionarydemo.databinding.FragmentUrbanDictionaryBinding
import com.ghn.urbandictionarydemo.shared.extensions.isNetworkAvailable
import com.ghn.urbandictionarydemo.ui.lookup.SharedLookUpViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

@ExperimentalCoroutinesApi
@FlowPreview
abstract class BaseLookupFragment : Fragment() {

    val viewModel by sharedViewModel<SharedLookUpViewModel>()

    lateinit var binding: FragmentUrbanDictionaryBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = FragmentUrbanDictionaryBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.networkConnected = requireContext().isNetworkAvailable()
        setUpObservers()
    }

    abstract fun setUpObservers()

    abstract fun setUpSortDialog()

    fun setSearchArea(query: String) {
        binding.searchTerm.text = when {
            viewModel.networkConnected -> {
                binding.searchTerm.setBackgroundResource(R.drawable.image_background)
                getString(R.string.search_term_formatter, query)
            }
            else -> {
                binding.searchTerm.background = null
                getString(R.string.network_not_connected)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_filter) {
            setUpSortDialog()
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun showLoading() {
        binding.progress.visibility = View.VISIBLE
    }

    protected fun showError() {
        binding.progress.visibility = View.INVISIBLE
        Toast.makeText(
            requireContext(),
            "An Error Occurred when retrieving search results",
            Toast.LENGTH_LONG
        ).show()
    }
}
