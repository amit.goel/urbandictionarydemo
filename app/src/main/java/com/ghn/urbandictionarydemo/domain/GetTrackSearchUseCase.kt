package com.ghn.urbandictionarydemo.domain

import com.ghn.urbandictionarydemo.data.models.response.tracksearch.TrackSearchResponse
import com.ghn.urbandictionarydemo.domain.repository.LastFmRepository
import com.ghn.urbandictionarydemo.shared.domain.SuspendUseCase
import com.ghn.urbandictionarydemo.shared.util.Result
import kotlinx.coroutines.CoroutineDispatcher

class GetTrackSearchUseCase(
    private val lastFmRepository: LastFmRepository,
    defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<String, TrackSearchResponse>(defaultDispatcher) {

    override suspend fun execute(parameters: String): TrackSearchResponse {
        return when (val result = lastFmRepository.trackSearch(parameters)) {
            is Result.Success -> result.data
            is Result.Error -> throw result.throwable
            else -> throw IllegalStateException()
        }
    }
}
