package com.ghn.urbandictionarydemo.domain.repository

import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule
import com.ghn.urbandictionarydemo.shared.util.Result

interface UrbanRepository {

    suspend fun getDefinitions(query: Pair<String, Boolean>): Result<List<DefinitionModule>>
}
