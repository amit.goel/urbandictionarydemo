package com.ghn.urbandictionarydemo.domain.repository

import com.ghn.urbandictionarydemo.data.models.response.tracksearch.TrackSearchResponse
import com.ghn.urbandictionarydemo.shared.util.Result

interface LastFmRepository {
    suspend fun trackSearch(query: String): Result<TrackSearchResponse>
}
