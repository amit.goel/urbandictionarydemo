package com.ghn.urbandictionarydemo.domain

import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule
import com.ghn.urbandictionarydemo.domain.repository.UrbanRepository
import com.ghn.urbandictionarydemo.shared.domain.SuspendUseCase
import com.ghn.urbandictionarydemo.shared.util.Result
import kotlinx.coroutines.CoroutineDispatcher

class GetUrbanDefinitionsUseCase(
    private val urbanRepository: UrbanRepository,
    defaultDispatcher: CoroutineDispatcher
) : SuspendUseCase<Pair<String, Boolean>, List<DefinitionModule>>(defaultDispatcher) {

    override suspend fun execute(parameters: Pair<String, Boolean>): List<DefinitionModule> {
        return when (val result = urbanRepository.getDefinitions(parameters)) {
            is Result.Success -> result.data
            is Result.Error -> throw result.throwable
            else -> throw IllegalStateException()
        }
    }
}
