package com.ghn.urbandictionarydemo.data.base

import com.ghn.urbandictionarydemo.data.models.response.definition.Definitions
import com.ghn.urbandictionarydemo.shared.util.Result

interface UrbanDataSource {
    suspend fun getDefinitions(query: String): Result<Definitions>
}
