package com.ghn.urbandictionarydemo.data.models.response.definition

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Definitions(
    @Json(name = "list") val list: List<Definition>
) : Parcelable
