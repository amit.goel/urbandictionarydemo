package com.ghn.urbandictionarydemo.data.base

import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule

interface UrbanLocalDataSource {
    suspend fun getDefinitions(query: String): List<DefinitionModule>

    suspend fun saveDefinitions(definitions: List<DefinitionModule>)
}
