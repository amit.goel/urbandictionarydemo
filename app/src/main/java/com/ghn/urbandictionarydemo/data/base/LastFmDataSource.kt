package com.ghn.urbandictionarydemo.data.base

import com.ghn.urbandictionarydemo.data.models.response.tracksearch.TrackSearchResponse
import com.ghn.urbandictionarydemo.shared.util.Result

interface LastFmDataSource {
    suspend fun trackSearch(query: String): Result<TrackSearchResponse>
}
