package com.ghn.urbandictionarydemo.data.models.response.tracksearch

import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class TrackSearchResponse(
    @field:Json(name = "results") val results: Results
) : Parcelable

@Parcelize
data class TrackMatches(
    @field:Json(name = "track") val track: List<Track>
) : Parcelable

@Parcelize
data class Track(
    @field:Json(name = "name") val name: String,
    @field:Json(name = "artist") val artist: String,
    @field:Json(name = "url") val url: String,
    @field:Json(name = "listeners") val listeners: String,
    @field:Json(name = "image") val image: List<Image>
) : Parcelable

@Parcelize
data class Results(
    @field:Json(name = "opensearch:Query") val openSearchQuery: OpenSearchQuery,
    @field:Json(name = "opensearch:totalResults") val openSearchTotalResults: String,
    @field:Json(name = "opensearch:startIndex") val openSearchStartIndex: String,
    @field:Json(name = "opensearch:itemsPerPage") val openSearchItemsPerPage: String,
    @field:Json(name = "trackmatches") val trackMatches: TrackMatches,
    @field:Json(name = "@attr") val attr: Attr
) : Parcelable

@Parcelize
data class OpenSearchQuery(
    @field:Json(name = "#text") val text: String,
    @field:Json(name = "role") val role: String,
    @field:Json(name = "startPage") val startPage: String
) : Parcelable

@Parcelize
data class Image(
    @field:Json(name = "#text") val text: String,
    @field:Json(name = "size") val size: String
) : Parcelable

@Parcelize
class Attr : Parcelable
