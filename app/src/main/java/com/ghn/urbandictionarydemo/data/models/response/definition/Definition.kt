package com.ghn.urbandictionarydemo.data.models.response.definition

import android.os.Parcelable
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import java.util.Locale

@Parcelize
data class Definition(
    @field:Json(name = "definition") val definition: String,
    @field:Json(name = "permalink") val permalink: String,
    @field:Json(name = "thumbs_up") val thumbsUp: Int,
    @field:Json(name = "sound_urls") val soundUrls: List<String>,
    @field:Json(name = "author") val author: String,
    @field:Json(name = "word") val word: String,
    @field:Json(name = "defid") val defid: Int,
    @field:Json(name = "current_vote") val currentVote: String,
    @field:Json(name = "written_on") val writtenOn: String,
    @field:Json(name = "example") val example: String,
    @field:Json(name = "thumbs_down") val thumbsDown: Int
) : Parcelable {

    fun mapToModule() = DefinitionModule(
        id = defid.toLong(),
        author = author,
        current_vote = currentVote,
        definition = definition,
        example = example,
        permalink = permalink,
        soundUrls = soundUrls.joinToString(),
        term = word.toLowerCase(Locale.getDefault()),
        thumbsUp = thumbsUp,
        thumbsDown = thumbsDown,
        writtenOn = writtenOn
    )
}
