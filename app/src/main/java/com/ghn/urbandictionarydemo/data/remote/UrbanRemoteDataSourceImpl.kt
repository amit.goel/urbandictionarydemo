package com.ghn.urbandictionarydemo.data.remote

import com.ghn.urbandictionarydemo.data.api.UrbanApi
import com.ghn.urbandictionarydemo.data.base.UrbanRemoteDataSource
import com.ghn.urbandictionarydemo.data.models.response.definition.Definitions
import com.ghn.urbandictionarydemo.shared.util.Result
import retrofit2.Response

class UrbanRemoteDataSourceImpl(
    private val api: UrbanApi
) : UrbanRemoteDataSource {

    override suspend fun getDefinitions(query: String): Result<Definitions> {
        val result = api.fetchDefinitions(query)
        return handleResult(result)
    }

    private fun <T> handleResult(result: Response<T>): Result<T> = when {
        result.isSuccessful -> result.body()?.let { body ->
            Result.Success(body)
        } ?: Result.Error(Exception("Expecting body"))
        else -> Result.Error(Exception("Remote Failure"))
    }
}
