package com.ghn.urbandictionarydemo.data.api

import com.ghn.urbandictionarydemo.data.models.response.tracksearch.TrackSearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface LastFmApi {
    @GET(".")
    suspend fun searchTracks(
        @Query("track") query: String,
        @Query("api_key") apiKey: String = "API_KEY",
        @Query("method") method: String = "track.search",
        @Query("format") format: String = "json",
        @Query("artist") artist: String? = null,
        @Query("limit") limit: String? = null,
        @Query("page") page: String? = null
    ): Response<TrackSearchResponse>
}
