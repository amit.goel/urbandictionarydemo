package com.ghn.urbandictionarydemo.data.models.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "definition_module")
data class DefinitionModule(
    @PrimaryKey(autoGenerate = false) var id: Long,
    @ColumnInfo(name = "definition") val definition: String,
    @ColumnInfo(name = "permalink") val permalink: String,
    @ColumnInfo(name = "thumbs_up") val thumbsUp: Int,
    @ColumnInfo(name = "sound_urls") val soundUrls: String,
    @ColumnInfo(name = "author") val author: String,
    @ColumnInfo(name = "term") val term: String,
    @ColumnInfo(name = "current_vote") val current_vote: String,
    @ColumnInfo(name = "written_on") val writtenOn: String,
    @ColumnInfo(name = "example") val example: String,
    @ColumnInfo(name = "thumbs_down") val thumbsDown: Int
)
