package com.ghn.urbandictionarydemo.data.repository

import com.ghn.urbandictionarydemo.data.base.UrbanLocalDataSource
import com.ghn.urbandictionarydemo.data.base.UrbanRemoteDataSource
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule
import com.ghn.urbandictionarydemo.domain.repository.UrbanRepository
import com.ghn.urbandictionarydemo.shared.util.Result
import java.util.Locale

class UrbanRepositoryImpl(
    private val localDataSource: UrbanLocalDataSource,
    private val remoteDataSource: UrbanRemoteDataSource
) : UrbanRepository {

    override suspend fun getDefinitions(query: Pair<String, Boolean>): Result<List<DefinitionModule>> {

        val term = query.first.toLowerCase(Locale.getDefault())
        val networkConnected = query.second

        if (!networkConnected) {
            return Result.Success(localDataSource.getDefinitions(term))
        }

        return when (val networkResult = remoteDataSource.getDefinitions(term)) {
            is Result.Success -> {
                val definitions = networkResult.data.list
                val definitionModules = definitions.map { it.mapToModule() }

                localDataSource.saveDefinitions(definitionModules)

                Result.Success(localDataSource.getDefinitions(term))
            }
            is Result.Error -> Result.Error(networkResult.throwable)
            is Result.Loading -> Result.Loading
        }
    }
}
