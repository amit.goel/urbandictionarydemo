package com.ghn.urbandictionarydemo.data.repository

import com.ghn.urbandictionarydemo.data.base.LastFmRemoteDataSource
import com.ghn.urbandictionarydemo.data.models.response.tracksearch.TrackSearchResponse
import com.ghn.urbandictionarydemo.domain.repository.LastFmRepository
import com.ghn.urbandictionarydemo.shared.util.Result
import java.util.Locale

class LastFmRepositoryImpl(
    private val remoteDataSource: LastFmRemoteDataSource
) : LastFmRepository {

    override suspend fun trackSearch(query: String): Result<TrackSearchResponse> {
        val term = query.toLowerCase(Locale.getDefault())
        return when (val networkResult = remoteDataSource.trackSearch(term)) {
            is Result.Success -> networkResult
            is Result.Error -> Result.Error(networkResult.throwable)
            is Result.Loading -> Result.Loading
        }
    }
}
