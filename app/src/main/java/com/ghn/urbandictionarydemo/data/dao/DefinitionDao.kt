package com.ghn.urbandictionarydemo.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule

@Dao
interface DefinitionDao : BaseDao<DefinitionModule> {

    @Query("SELECT * FROM definition_module  where term = :term")
    suspend fun getAll(term: String): List<DefinitionModule>
}
