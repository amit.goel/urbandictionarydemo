package com.ghn.urbandictionarydemo.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ghn.urbandictionarydemo.data.dao.DefinitionDao
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule

@Database(
    entities = [
        (DefinitionModule::class)
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun definitionDao(): DefinitionDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        private const val DB_NAME = "urbanDb"

        fun getInstance(context: Context): AppDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                .build()
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}
