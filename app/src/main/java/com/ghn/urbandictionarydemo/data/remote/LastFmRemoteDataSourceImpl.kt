package com.ghn.urbandictionarydemo.data.remote

import com.ghn.urbandictionarydemo.data.api.LastFmApi
import com.ghn.urbandictionarydemo.data.base.LastFmRemoteDataSource
import com.ghn.urbandictionarydemo.data.models.response.tracksearch.TrackSearchResponse
import com.ghn.urbandictionarydemo.shared.util.Result
import retrofit2.Response

class LastFmRemoteDataSourceImpl(
    private val api: LastFmApi
) : LastFmRemoteDataSource {

    override suspend fun trackSearch(query: String): Result<TrackSearchResponse> {
        val result = api.searchTracks(query)
        return handleResult(result)
    }

    private fun <T> handleResult(result: Response<T>): Result<T> = when {
        result.isSuccessful -> result.body()?.let { body ->
            Result.Success(body)
        } ?: Result.Error(Exception("Expecting body"))
        else -> Result.Error(Exception("Remote Failure"))
    }
}
