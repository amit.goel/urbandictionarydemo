package com.ghn.urbandictionarydemo.data.api

import com.ghn.urbandictionarydemo.BuildConfig
import com.ghn.urbandictionarydemo.data.models.response.definition.Definitions
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

private const val URBAN_HOST = "mashape-community-urban-dictionary.p.rapidapi.com"

interface UrbanApi {

    @Headers(
        "x-rapidapi-host: $URBAN_HOST",
        "x-rapidapi-key: ${BuildConfig.URBAN_API_KEY}"
    )
    @GET("define")
    suspend fun fetchDefinitions(
        @Query("term") query: String
    ): Response<Definitions>
}
