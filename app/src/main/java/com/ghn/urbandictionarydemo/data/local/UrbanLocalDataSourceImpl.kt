package com.ghn.urbandictionarydemo.data.local

import com.ghn.urbandictionarydemo.data.AppDatabase
import com.ghn.urbandictionarydemo.data.base.UrbanLocalDataSource
import com.ghn.urbandictionarydemo.data.models.entity.DefinitionModule

class UrbanLocalDataSourceImpl(
    private val database: AppDatabase
) : UrbanLocalDataSource {
    override suspend fun saveDefinitions(definitions: List<DefinitionModule>) {
        database.definitionDao().insert(definitions)
    }

    override suspend fun getDefinitions(query: String): List<DefinitionModule> {
        return database.definitionDao().getAll(query)
    }
}
