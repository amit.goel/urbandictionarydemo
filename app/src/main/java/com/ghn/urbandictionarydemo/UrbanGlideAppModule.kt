package com.ghn.urbandictionarydemo

import android.content.Context
import android.util.Log
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions

@GlideModule
class UrbanGlideAppModule : AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        super.applyOptions(context, builder)

        builder.apply {
            if (BuildConfig.DEBUG) setLogLevel(Log.INFO)
            RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
        }
    }
}
