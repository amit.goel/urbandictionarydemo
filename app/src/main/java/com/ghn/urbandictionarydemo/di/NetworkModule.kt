package com.ghn.urbandictionarydemo.di

import com.ghn.urbandictionarydemo.BuildConfig
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { provideHTTPLoggingInterceptor() }
    single { provideDefaultOkHttpClient(get()) }

    single(named("dispatchersIO")) { provideDispatchersIO() }
    single(named("urbanRetro")) { provideRetrofit(get(), BuildConfig.HOST) }
    single(named("lastFmRetro")) { provideRetrofit(get(), BuildConfig.LAST_FM_HOST) }
}

private fun provideDispatchersIO() = Dispatchers.IO

private fun provideHTTPLoggingInterceptor(): HttpLoggingInterceptor =
    HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

private fun provideDefaultOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient {

    val okHttpClientBuilder = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .connectTimeout(30.toLong(), TimeUnit.SECONDS)
        .readTimeout(60.toLong(), TimeUnit.SECONDS)
        .writeTimeout(60.toLong(), TimeUnit.SECONDS)
        .callTimeout(180.toLong(), TimeUnit.SECONDS)

    return okHttpClientBuilder.build()
}

private fun provideRetrofit(okHttpClient: OkHttpClient, host: String): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create())
        .baseUrl(host)
        .client(okHttpClient)
        .build()
}
