package com.ghn.urbandictionarydemo.di

import com.ghn.urbandictionarydemo.data.api.LastFmApi
import com.ghn.urbandictionarydemo.data.api.UrbanApi
import com.ghn.urbandictionarydemo.data.base.LastFmRemoteDataSource
import com.ghn.urbandictionarydemo.data.base.UrbanLocalDataSource
import com.ghn.urbandictionarydemo.data.base.UrbanRemoteDataSource
import com.ghn.urbandictionarydemo.data.local.UrbanLocalDataSourceImpl
import com.ghn.urbandictionarydemo.data.remote.LastFmRemoteDataSourceImpl
import com.ghn.urbandictionarydemo.data.remote.UrbanRemoteDataSourceImpl
import com.ghn.urbandictionarydemo.data.repository.LastFmRepositoryImpl
import com.ghn.urbandictionarydemo.data.repository.UrbanRepositoryImpl
import com.ghn.urbandictionarydemo.domain.GetTrackSearchUseCase
import com.ghn.urbandictionarydemo.domain.GetUrbanDefinitionsUseCase
import com.ghn.urbandictionarydemo.domain.repository.LastFmRepository
import com.ghn.urbandictionarydemo.domain.repository.UrbanRepository
import com.ghn.urbandictionarydemo.ui.lookup.SharedLookUpViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

@ExperimentalCoroutinesApi
val urbanLookupModule = module {

    single { createRetrofit<UrbanApi>(retrofit = get(named("urbanRetro"))) }
    single { createRetrofit<LastFmApi>(retrofit = get(named("lastFmRetro"))) }

    single(named<UrbanRemoteDataSource>()) { UrbanRemoteDataSourceImpl(get()) }
    single(named<UrbanLocalDataSource>()) { UrbanLocalDataSourceImpl(get()) }

    single(named<LastFmRemoteDataSource>()) { LastFmRemoteDataSourceImpl(get()) }

    single(named<UrbanRepository>()) {
        UrbanRepositoryImpl(
            localDataSource = get(named<UrbanLocalDataSource>()),
            remoteDataSource = get(named<UrbanRemoteDataSource>())
        )
    }

    single(named<LastFmRepository>()) {
        LastFmRepositoryImpl(get(named<LastFmRemoteDataSource>()))
    }

    factory {
        GetUrbanDefinitionsUseCase(
            urbanRepository = get(named<UrbanRepository>()),
            defaultDispatcher = get(named("dispatchersIO"))
        )
    }

    factory {
        GetTrackSearchUseCase(
            lastFmRepository = get(named<LastFmRepository>()),
            defaultDispatcher = get(named("dispatchersIO"))
        )
    }

    viewModel { SharedLookUpViewModel(get(), get()) }
}

inline fun <reified T> createRetrofit(retrofit: Retrofit): T = retrofit.create(T::class.java)
