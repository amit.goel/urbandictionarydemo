package com.ghn.urbandictionarydemo.di

import android.content.Context
import com.ghn.urbandictionarydemo.data.AppDatabase
import org.koin.dsl.module

val localModule = module {
    single { provideRoom(get()) }
}

private fun provideRoom(context: Context): AppDatabase = AppDatabase.getInstance(context)
