Urban Dictionary Lookup Demo App
==============================================
### Technologies used

This sample is written in Kotlin and uses modern MVVM architecture and uses Coroutines and flow to
help with asynchronous access of data. [master](https://gitlab.com/amit.goel/urbandictionarydemo/commits/master) branch which uses
the following Architecture Components:
 - ViewModel and LiveData
 - Navigation
 - Room for data persistence
 - Coroutines and Flow 
 - Built using Kotlin Build Script 
 - Koin for Dependency Injection
 - Data, Domain, and presentation layers for clear separation of concerns
 - Retrofit and Moshi to help with Remote data
 - Create Unit and Espresso Tests for the various layers 
 
## Notes
 - Provides Sorting options of results
 - Shows progress loading indicator when searching
 - Includes Unit and instrumented tests
 - Handles device rotation well, and viewmodel retains specific data
 - Data is persisted for limited offline usage
 - uses Dependency Injection 
  
## TODO List
 - Handle Error states more thoroughly
 - Handle Saved Instance state
 - Add additional unit and instrumentation tests 
 - Improve the UI, provide better Image Assets, and extract styles for common views
 - Setup CI configuration for tests
 - Code cleanup and Lint Checks
 - Improve Recycler View Adapters that handle multiple view types
 
## Assumptions
 - Handles offline use case by checking for network state.  Further work may need to be done to 
 check if internet connection is available, and mroe tests to determine behavior or server timeouts
 - Provided default search of "moonshot".  This would be changed to a word of the day for default listings
 - Advertisements or alternate viewtypes for recyclerview can be provided as needed with adjustments to adapter
 -  
 
### Screenshots
![alt text](art/Screenshot_1580449042.png "Search")


