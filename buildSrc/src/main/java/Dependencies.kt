@Suppress("SpellCheckingInspection")
object Apps {
    const val compileSdk = 29
    const val minSdk = 21
    const val targetSdk = 29
    const val versionCode = 1
    const val versionName = "0.0.1"
    const val buildTools = "29.0.2"
    const val applicationId = "com.ghn.urbandictionarydemo"
}

@Suppress("SpellCheckingInspection")
object Versions {
    const val gradle = "4.0.0"
    const val kotlin = "1.3.61"
    const val appcompat = "1.1.0"
    const val browser = "1.2.0"
    const val constraintLayout = "1.1.3"
    const val fragment = "1.2.0"
    const val material = "1.2.0-alpha02"
    const val navigation = "2.1.0"
    const val safeArgs = "2.2.0"

    const val okHttp = "4.3.0"
    const val retrofit = "2.7.1"

    const val koin = "2.0.1"

    const val coroutines = "1.3.3"

    const val room = "2.2.2"
    const val lifecycleExtensions = "2.2.0-rc03"

    const val glide = "4.10.0"
    const val timber = "4.7.1"

    /* test */
    const val junit = "4.12"
    const val junitAndroid = "1.1.1"
    const val espresso = "3.2.0"
    const val testCore = "1.2.0"
    const val coroutinesTest = "1.3.2"
    const val mockWebServer = "4.2.2"
}

@Suppress("SpellCheckingInspection")
object Libs {

    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"

    const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.appcompat}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"

    const val fragment = "androidx.fragment:fragment-ktx:${Versions.fragment}"
    const val navigation = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
    const val navigationUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"

    const val okHttpInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"

    const val koin = "org.koin:koin-android:${Versions.koin}"
    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"

    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}"
    const val coroutinesCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}"

    const val room = "androidx.room:room-ktx:${Versions.room}"
    const val roomRuntime = "androidx.room:room-runtime:${Versions.room}"
    const val roomKapt = "androidx.room:room-compiler:${Versions.room}"

    const val lifecycleExtensions =
        "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleExtensions}"
    const val lifecycleLiveData =
        "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycleExtensions}"
    const val lifecycleViewModel =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycleExtensions}"
    const val lifecycleRuntime =
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleExtensions}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
}

@Suppress("SpellCheckingInspection")
object Hosts {

    val baseUrl = format("https://mashape-community-urban-dictionary.p.rapidapi.com/")
    val lastFmBaseUrl = format("https://ws.audioscrobbler.com/2.0/")
    val urbanApiKey = format("10f3f569damsh590b8f915534d90p1abb7ajsnfe77239b047c")

    private fun format(host: String) = "\"$host\""
}

@Suppress("SpellCheckingInspection")
object TestLibs {
    const val junit = "junit:junit:${Versions.junit}"
    const val junitAndroid = "androidx.test.ext:junit:${Versions.junitAndroid}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
    const val fragmentTest = "androidx.fragment:fragment-testing:${Versions.fragment}"
    const val testCore = "androidx.test:core:${Versions.testCore}"
    const val testRules = "androidx.test:rules:${Versions.testCore}"
    const val testRunner = "androidx.test:runner:${Versions.testCore}"

    // TODO: test dependencies to add in later
    const val koinTest = "org.koin:koin-test:${Versions.koin}"
    const val coroutinesTest =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesTest}"

    const val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.mockWebServer}"
}
